$(function () {
    $("#datepicker").datepicker();
})

$(document).on("click", "#submit", function () {
        $(".result").text(getDays($("#datepicker").val()))
})

function getDays(date){
    var selectedDateString = date;

    var currentDate = new Date();
    var selectedDate = new Date(selectedDateString);
    if (selectedDateString === "" || selectedDateString === null) {
        throw Error("Please select Date")
    }
    else if (currentDate.getTime() - selectedDate.getTime() > 0)
        return Math.round((currentDate.getTime() - selectedDate.getTime()) / (1000 * 60 * 60 * 24)) - 1 + " days behind"
    else
        return Math.round(-(currentDate.getTime() - (1000 * 60 * 60 * 24) - selectedDate.getTime()) / (1000 * 60 * 60 * 24)) + " days forward"
}