$(function () {



    $('#contact-form').on('submit', function (e) {

        // if the validator does not prevent form submit
        if (!e.isDefaultPrevented()) {


            var alertBox = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Contact form successfully submitted. Thank you, I will get back to you soon! </div>';

            // If we have messageAlert and messageText

                // inject the alert to .messages div in our form
                $('#contact-form').find('.messages').html(alertBox);
                // $('#contact-form')[1].reset();

        }
    })
});