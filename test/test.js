

QUnit.test('Testing getDays function with several sets of inputs', function (assert) {
    let date = new Date()
    assert.equal(getDays(`${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`), "0 days behind", 'Tested with Todays Date')
    assert.equal(getDays(`${date.getMonth() + 1}/${date.getDate() - 20}/${date.getFullYear()}`), "20 days behind", 'Tested with Another Date')
    assert.throws(function () { getDays("") },/Please select Date/ ,'Tested with no Date')
    assert.throws(function () { getDays(null) }, /Please select Date/, 'Tested with null')
//   assert.equal(App.calculateArea(5, 5), 25, 'Tested with two relatively small positive numbers');
//   assert.equal(App.calculateArea(500, 500), 250000, 'Tested with two large positive numbers. ');
//   assert.equal(App.calculateArea(-5, -5), 1, 'Tested with two negative numbers. Any arguments less than 1 will be set to 1.');
//   assert.throws(function () { App.calculateArea('a', 'b'); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');
});